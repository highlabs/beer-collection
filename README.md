This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.js`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

This is a basic project to showing the uses of the [`PunkApi`](https://punkapi.com/). The uses only Next.js, CSS Modules and Zustand.

You will be able to:

- See a list of beers
- See a beer details
- Search for a beer Name, ABV, IBU, EBC, Yeast, Hops, Malt and Food Pairing.
- Add new beers to the local storage
- See your added beers (but not the details yet)

What are planned, but is missing

- Date related search
- Image upload on add new beer

As the PunkApi has no information how to add Beers, I'm just saving them in the local storage.

I choose not to use any UI framework like Tailwind or Material UI just to show my knowledge about CSS and HTML. But the design is simple and is made just to work on any screen size.
