import React from "react";
import Image from "next/image";
import style from "./beerInfo.module.css";

const BeerInfo = ({ beer, showImage }) => {
  return (
    <div className={style.infoContainer}>
      {showImage && (
        <div>
          <div className={style.imageInfo}>
            <Image
              src={beer.image_url}
              fill={true}
              alt={`Picture of the beer ${beer.name}`}
              style={{
                objectFit: "contain",
              }}
            />
          </div>
        </div>
      )}
      <div className={style.beerInfo}>
        <h1>
          {beer.name} - <small>{beer.tagline}</small>
        </h1>
        <p>{beer.description}</p>
        <p>
          <strong>Brewer&lsquo;s Tips</strong>: {beer.brewers_tips}
        </p>

        <div className={style.beerNutrition}>
          <h2>Technical Information</h2>
          <hr />
          <h4 className={style.serving}>
            <span>Serving size</span>{" "}
            <span>
              {beer.volume.value} {beer.volume.unit}
            </span>
          </h4>
          <hr className={style.hr} />
          <ul className="inline">
            <li>
              <strong>ABV</strong>: {beer.abv}%
            </li>
            <li>
              <strong>IBU</strong>: {beer.ibu}
            </li>
            <li>
              <strong>EBC</strong>: {beer.ebc}
            </li>
            <li>
              <strong>SRM</strong>: {beer.srm}
            </li>
            <li>
              <strong>pH</strong>: {beer.ph}
            </li>
            <li>
              <strong>Target FG</strong>: {beer.target_fg}
            </li>
            <li>
              <strong>Target OG</strong>: {beer.target_og}
            </li>
          </ul>
          <h3>Ingredients</h3>
          <p>
            <strong>Malt</strong>:
          </p>
          <ul>
            {beer.ingredients.malt.map((malt) => (
              <li key={malt.name + malt.amount.value + malt.amount.unit}>
                {malt.name}: {malt.amount.value} {malt.amount.unit}
              </li>
            ))}
          </ul>
          <p>
            <strong>Hops</strong>:
          </p>
          <ul>
            {beer.ingredients.hops.map((hop) => (
              <li key={hop.name + hop.amount.value + hop.amount.unit}>
                {hop.name}: {hop.amount.value} {hop.amount.unit} - {hop.add} (
                {hop.attribute})
              </li>
            ))}
          </ul>
          <p>
            <strong>Yeast</strong>:
          </p>

          <ul>
            <li>{beer.ingredients.yeast}</li>
          </ul>
          <h3>Food Pairing:</h3>
          <ul>
            {beer.food_pairing.map((food) => (
              <li key={food}>{food}</li>
            ))}
          </ul>
          <h3>Method:</h3>
          <ul>
            {beer.method.mash_temp.map((mash) => (
              <li key={mash.temp.value + mash.temp.unit}>
                Mash at {mash.temp.value} {mash.temp.unit} for {mash.duration}{" "}
                minutes
              </li>
            ))}
            <li>
              Fermented at {beer.method.fermentation.temp.value}{" "}
              {beer.method.fermentation.temp.unit}
            </li>
            <li>
              {beer.method.twist ? `Twist: ${beer.method.twist}` : "No twist"}
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default BeerInfo;
