"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";
import style from "./searchBeer.module.css";

function SearchBeer({ onSearch }) {
  const [searchText, setSearchText] = useState("");
  const router = useRouter();

  const handleSearch = () => {
    router.push(`/search/${searchText}`);
  };

  return (
    <div className={style.formControl}>
      <input
        type="text"
        value={searchText}
        onChange={(e) => setSearchText(e.target.value)}
        className={style.input}
        placeholder="Search beer by name"
      />
      <button onClick={handleSearch} className={style.inputButton}>
        Search
      </button>
    </div>
  );
}

export default SearchBeer;
