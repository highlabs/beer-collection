"use client";
import { useState } from "react";
import BeerGrid from "@/components/beerGrid";
import Sidebar from "@/components/sidebar";
import style from "./beerContainer.module.css";

export default function BeerContainer({ data }) {
  const [beerList, setBeerList] = useState(data);

  const handleSearch = async (qs) => {
    const res = await fetch("https://api.punkapi.com/v2/beers" + qs);

    if (!res.ok) {
      throw new Error(res.statusText);
    }

    const data = await res.json();
    setBeerList(data);
  };

  return (
    <div className={style.beerContainer}>
      <Sidebar onSearch={handleSearch} />
      <BeerGrid beerList={beerList} />
    </div>
  );
}
