import Image from "next/image";
import styles from "./beerCard.module.css";
import notFoundImage from "../../../public/not-found.jpg";

const BeerCard = ({ name, tagline, image_url, id, abv, ibu }) => {
  return (
    <div className={styles.card}>
      <div>
        <h2>{name}</h2>
        <h4>{tagline}</h4>
        <ul className="inline">
          <li className={styles.item}>
            <strong>ABV</strong>: {abv || "N/A"}
          </li>
          <li className={styles.item}>
            <strong>IBU</strong>: {ibu || "N/A"}
          </li>
        </ul>
      </div>
      <div className={styles.cardImageContainer}>
        <Image
          src={image_url || notFoundImage}
          fill={true}
          alt={`Image of the beer: ${name}`}
          style={{ objectFit: "contain" }}
          sizes="100%"
        />
      </div>
    </div>
  );
};

export default BeerCard;
