"use client";

import { useState } from "react";
import style from "./sidebar.module.css";

const Sidebar = ({ onSearch }) => {
  const [showAdvancedSearch, setShowAdvancedSearch] = useState(false);
  const [searchParams, setSearchParams] = useState({
    abv_gt: "",
    abv_lt: "",
    ibu_gt: "",
    ibu_lt: "",
    ebc_gt: "",
    ebc_lt: "",
    beer_name: "",
    yeast: "",
    hops: "",
    malt: "",
    food: "",
  });

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setSearchParams({ ...searchParams, [name]: value });
  };

  function removeEmptyEntries(obj) {
    const newObj = {};
    for (const [key, value] of Object.entries(obj)) {
      if (value !== "") {
        newObj[key] = value;
      }
    }
    return newObj;
  }

  const handleSearch = () => {
    const params = removeEmptyEntries(searchParams);
    const qs = "?" + new URLSearchParams(params).toString();

    onSearch(qs);
  };

  return (
    <div className={style.sidebar}>
      <h2>Search Beers</h2>
      <form className="form">
        <div className="formControl">
          <label htmlFor="beer_name">Beer Name:</label>
          <input
            type="text"
            name="beer_name"
            className="input"
            value={searchParams.beer_name}
            onChange={handleInputChange}
          />
        </div>
        <div className={!showAdvancedSearch && style.advancedSearchHidden}>
          <div className="formControl">
            <label htmlFor="abv_gt">ABV Greater Than:</label>
            <input
              type="number"
              name="abv_gt"
              className="input"
              value={searchParams.abv_gt}
              max={searchParams.abv_lt}
              onChange={handleInputChange}
            />
          </div>

          <div className="formControl">
            <label htmlFor="abv_lt">ABV Less Than:</label>
            <input
              type="number"
              name="abv_lt"
              className="input"
              value={searchParams.abv_lt}
              min={searchParams.abv_gt || 0}
              onChange={handleInputChange}
            />
          </div>

          <div className="formControl">
            <label htmlFor="ibu_gt">IBU Greater Than:</label>
            <input
              type="number"
              name="ibu_gt"
              className="input"
              value={searchParams.ibu_gt}
              max={searchParams.ibu_lt}
              onChange={handleInputChange}
            />
          </div>

          <div className="formControl">
            <label htmlFor="ibu_lt">IBU Less Than:</label>
            <input
              type="number"
              name="ibu_lt"
              className="input"
              value={searchParams.ibu_lt}
              min={searchParams.ibu_gt || 0}
              onChange={handleInputChange}
            />
          </div>

          <div className="formControl">
            <label htmlFor="ebc_gt">EBC Greater Than:</label>
            <input
              type="number"
              name="ebc_gt"
              className="input"
              value={searchParams.ebc_gt}
              max={searchParams.ebc_lt}
              onChange={handleInputChange}
            />
          </div>

          <div className="formControl">
            <label htmlFor="ebc_lt">EBC Less Than:</label>
            <input
              type="number"
              name="ebc_lt"
              className="input"
              value={searchParams.ebc_lt}
              min={searchParams.ebc_gt || 0}
              onChange={handleInputChange}
            />
          </div>

          <div className="formControl">
            <label htmlFor="yeast">Yeast:</label>
            <input
              type="text"
              name="yeast"
              className="input"
              value={searchParams.yeast}
              onChange={handleInputChange}
            />
          </div>

          <div className="formControl">
            <label htmlFor="hops">Hops:</label>
            <input
              type="text"
              name="hops"
              className="input"
              value={searchParams.hops}
              onChange={handleInputChange}
            />
          </div>

          <div className="formControl">
            <label htmlFor="malt">Malt:</label>
            <input
              type="text"
              name="malt"
              className="input"
              value={searchParams.malt}
              onChange={handleInputChange}
            />
          </div>

          <div className="formControl">
            <label htmlFor="food">Food:</label>
            <input
              type="text"
              name="food"
              className="input"
              value={searchParams.food}
              onChange={handleInputChange}
            />
          </div>
        </div>
        <button
          type="button"
          onClick={() => setShowAdvancedSearch(!showAdvancedSearch)}
          className={style.showAdvancedSearchButton}
        >
          {showAdvancedSearch ? "Hide" : "Show"} Advanced Search
        </button>

        <button type="button" onClick={handleSearch}>
          Search
        </button>
      </form>
    </div>
  );
};

export default Sidebar;
