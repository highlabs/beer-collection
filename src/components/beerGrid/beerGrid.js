import styles from "./beerGrid.module.css";
import BeerCard from "@/components/beerCard";
import Link from "next/link";

export default function BeerGrid({ beerList, noLink }) {
  if (beerList.length === 0) {
    return (
      <div className={styles.notFound}>
        <h1>No beers found.</h1>
      </div>
    );
  }

  if (noLink) {
    return (
      <div className={styles.beerGrid}>
        {beerList?.map((beer) => (
          <BeerCard {...beer} key={beer.id} />
        ))}
      </div>
    );
  }

  return (
    <div className={styles.beerGrid}>
      {beerList?.map((beer) => (
        <Link href={`/beer/${beer.id}`} key={beer.id}>
          <BeerCard {...beer} />
        </Link>
      ))}
    </div>
  );
}
