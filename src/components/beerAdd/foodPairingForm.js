"use client";
import { useState } from "react";

const FoodPairingForm = ({ beer, setBeer }) => {
  const [foodPairing, setFoodPairing] = useState("");

  const addFoodPairing = (event) => {
    event.preventDefault();
    setBeer({
      ...beer,
      food_pairing: [...beer.food_pairing, foodPairing],
    });
    setFoodPairing("");
  };

  return (
    <div className="formControl">
      <h4>Food pairing</h4>
      <ul>
        {beer.food_pairing.map((food, index) => (
          <li key={index}>{food}</li>
        ))}
      </ul>
      <div className="formControl">
        <label htmlFor="food_pairing">Food</label>
        <input
          className="input"
          type="text"
          id="food_pairing"
          value={foodPairing}
          onChange={(e) => setFoodPairing(e.target.value)}
        />
      </div>
      <button onClick={addFoodPairing}>Add Food Pairing</button>
    </div>
  );
};

export default FoodPairingForm;
