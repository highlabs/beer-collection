"use client";
import { useState } from "react";
import style from "./beerAdd.module.css";
import BeerInfo from "@/components/beerInfo";
import FoodPairingForm from "./foodPairingForm";
import MaltForm from "./maltForm";
import HopsForm from "./hopsForm";
import MashForm from "./mashForm";
import { useBeerStore } from "@/useBeerStore";
import { useRouter } from "next/navigation";

const DEFAULT_BEER_STATE = {
  name: "",
  tagline: "",
  description: "",
  image_url: "",
  abv: 0,
  ibu: 0,
  target_fg: 0,
  target_og: 0,
  ebc: 0,
  srm: 0,
  ph: 0,
  attenuation_level: 0,
  volume: { value: 0, unit: "litres" },
  boil_volume: { value: 0, unit: "litres" },
  method: {
    mash_temp: [],
    fermentation: { temp: { value: 0, unit: "celsius" } },
    twist: "",
  },
  ingredients: {
    malt: [],
    hops: [],
    yeast: "",
  },
  brewers_tips: "",
  contributed_by: "",
  food_pairing: [],
};

const BeerAdd = () => {
  const [beer, setBeer] = useState(DEFAULT_BEER_STATE);
  const addBeer = useBeerStore((state) => state.addBeer);
  const router = useRouter();

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(addBeer);
    addBeer(beer);
    setBeer(DEFAULT_BEER_STATE);
    router.push("/my_beers");
  };
  return (
    <main>
      <h1>Add new beer</h1>

      <div className={style.add_container}>
        <div className={style.form}>
          <form className="form">
            <div className="formControl">
              <label htmlFor="name">Name</label>
              <input
                type="text"
                id="name"
                className="input"
                value={beer.name}
                onChange={(e) => {
                  setBeer({ ...beer, name: e.target.value });
                }}
              />
            </div>
            <div className="formControl">
              <label htmlFor="tagline">Tagline</label>
              <input
                type="text"
                id="tagline"
                className="input"
                value={beer.tagline}
                onChange={(e) => {
                  setBeer({ ...beer, tagline: e.target.value });
                }}
              />
            </div>
            <div className="formControl">
              <label htmlFor="description">Description</label>
              <textarea
                type="text"
                id="description"
                className="input"
                value={beer.description}
                onChange={(e) => {
                  setBeer({ ...beer, description: e.target.value });
                }}
              />
            </div>
            {/* <div className="formControl">
              <label htmlFor="image_url">Image</label>
              <input
                type="file"
                id="image_url"
                accept="image/png, image/jpeg"
                className="input"
                value={beer.image_url}
                onChange={(e) => {
                  setBeer({ ...beer, image_url: e.target.value });
                }}
              />
            </div> */}
            <div className="formControl">
              <label htmlFor="abv">ABV</label>
              <input
                type="number"
                id="abv"
                className="input"
                value={beer.abv}
                onChange={(e) => {
                  setBeer({ ...beer, abv: e.target.value });
                }}
              />
            </div>
            <div className="formControl">
              <label htmlFor="ibu">IBU</label>
              <input
                type="number"
                id="ibu"
                className="input"
                value={beer.ibu}
                onChange={(e) => {
                  setBeer({ ...beer, ibu: e.target.value });
                }}
              />
            </div>
            <div className="formControl">
              <label htmlFor="target_fg">Target FG</label>
              <input
                type="number"
                id="target_fg"
                className="input"
                value={beer.target_fg}
                onChange={(e) => {
                  setBeer({ ...beer, target_fg: e.target.value });
                }}
              />
            </div>
            <div className="formControl">
              <label htmlFor="target_og">Target OG</label>
              <input
                type="number"
                id="target_og"
                className="input"
                value={beer.target_og}
                onChange={(e) => {
                  setBeer({ ...beer, target_og: e.target.value });
                }}
              />
            </div>
            <div className="formControl">
              <label htmlFor="ebc">EBC</label>
              <input
                type="number"
                id="ebc"
                className="input"
                value={beer.ebc}
                onChange={(e) => {
                  setBeer({ ...beer, ebc: e.target.value });
                }}
              />
            </div>
            <div className="formControl">
              <label htmlFor="srm">SRM</label>
              <input
                type="number"
                id="srm"
                className="input"
                value={beer.srm}
                onChange={(e) => {
                  setBeer({ ...beer, srm: e.target.value });
                }}
              />
            </div>
            <div className="formControl">
              <label htmlFor="ph">pH</label>
              <input
                type="number"
                id="ph"
                className="input"
                value={beer.ph}
                onChange={(e) => {
                  setBeer({ ...beer, ph: e.target.value });
                }}
              />
            </div>
            <div className="formControl">
              <label htmlFor="attenuation_level">Attenuation Level</label>
              <input
                type="number"
                id="attenuation_level"
                className="input"
                value={beer.attenuation_level}
                onChange={(e) => {
                  setBeer({ ...beer, attenuation_level: e.target.value });
                }}
              />
            </div>
            <div className="formControl">
              <label htmlFor="volume">Volume (in litres)</label>
              <input
                type="number"
                id="volume"
                className="input"
                value={beer.volume.value}
                onChange={(e) => {
                  setBeer({
                    ...beer,
                    volume: { unit: "litres", value: e.target.value },
                  });
                }}
              />
            </div>
            <div className="formControl">
              <label htmlFor="boil_volume">Boil Volume (in litres)</label>
              <input
                type="number"
                id="boil_volume"
                className="input"
                value={beer.boil_volume.value}
                onChange={(e) => {
                  setBeer({
                    ...beer,
                    boil_volume: { ...beer.boil_volume, value: e.target.value },
                  });
                }}
              />
            </div>
            <div className="formControl">
              <label htmlFor="fermentation_temp_value">
                Fermentation Temp (in celcius)
              </label>
              <input
                type="number"
                className="input"
                id="fermentation_temp_value"
                value={beer.method.fermentation.temp.value}
                onChange={(e) =>
                  setBeer({
                    ...beer,
                    method: {
                      ...beer.method,
                      fermentation: {
                        temp: {
                          unit: "celsius",
                          value: e.target.value,
                        },
                      },
                    },
                  })
                }
              />
            </div>
            <div className="formControl">
              <label htmlFor="twist">Twist</label>
              <input
                type="text"
                className="input"
                id="twist"
                value={beer.method.fermentation.twist}
                onChange={(e) =>
                  setBeer({
                    ...beer,
                    method: {
                      ...beer.method,
                      twist: e.target.value,
                    },
                  })
                }
              />
            </div>
            <div className="formControl">
              <label htmlFor="yeast">Yeast</label>
              <input
                className="input"
                type="text"
                id="yeast"
                value={beer.ingredients.yeast}
                onChange={(e) =>
                  setBeer({
                    ...beer,
                    ingredients: {
                      ...beer.ingredients,
                      yeast: e.target.value,
                    },
                  })
                }
              />
            </div>
            <div className="formControl">
              <label htmlFor="brewers_tips">Brewers Tips</label>
              <input
                className="input"
                type="text"
                id="brewers_tips"
                value={beer.brewers_tips}
                onChange={(e) =>
                  setBeer({ ...beer, brewers_tips: e.target.value })
                }
              />
            </div>
            <div className="formControl">
              <label htmlFor="contributed_by">Contributed By</label>
              <input
                className="input"
                type="text"
                id="contributed_by"
                value={beer.contributed_by}
                onChange={(e) =>
                  setBeer({ ...beer, contributed_by: e.target.value })
                }
              />
            </div>
            <MashForm beer={beer} setBeer={setBeer} />
            <HopsForm beer={beer} setBeer={setBeer} />
            <MaltForm beer={beer} setBeer={setBeer} />
            <FoodPairingForm beer={beer} setBeer={setBeer} />
            <input type="submit" value="Submit" onClick={handleSubmit} />
          </form>
        </div>
        <div className={style.labelContainer}>
          <div className={style.infoSticky}>
            <BeerInfo beer={beer} />
          </div>
        </div>
      </div>
    </main>
  );
};

export default BeerAdd;
