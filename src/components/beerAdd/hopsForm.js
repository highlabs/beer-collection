"use client";
import { useState } from "react";
import style from "./beerAdd.module.css";

const HopsForm = ({ beer, setBeer }) => {
  const [hopsTemp, setHopsTemp] = useState({
    name: "",
    amount: { value: 0, unit: "grams" },
    add: "",
    attribute: "",
  });

  const handleAddHopsTemp = (event) => {
    event.preventDefault();

    setBeer({
      ...beer,
      ingredients: {
        ...beer.ingredients,
        hops: [
          ...beer.ingredients.hops,
          {
            name: hopsTemp.name,
            amount: { ...hopsTemp.amount },
            add: hopsTemp.add,
            attribute: hopsTemp.attribute,
          },
        ],
      },
    });
    setHopsTemp({
      name: "",
      amount: { value: 0, unit: "grams" },
      add: "",
      attribute: "",
    });
  };

  const handleRemoveHops = (event, index) => {
    event.preventDefault();
    const newHopsTemp = [...beer.ingredients.hops];

    newHopsTemp.splice(index, 1);
    setBeer({
      ...beer,
      ingredients: {
        ...beer.ingredients,
        hops: newHopsTemp,
      },
    });
  };
  return (
    <div className="formControl">
      <div className="formControl">
        <p>
          <strong>Hops</strong>
        </p>
      </div>
      {beer.ingredients.hops.map((hop, index) => (
        <div className="formControl" key={index}>
          <p>{`Name: ${hop.name} / Amount: ${hop.amount.value}grams`}</p>
          <p>{`Add: ${hop.add} / Atribute: ${hop.attribute}`}</p>
          <button onClick={(e) => handleRemoveHops(e, index)}>
            Remove Hop
          </button>
        </div>
      ))}
      <div className="formControl">
        <label htmlFor="hops_name">Hops name</label>
        <input
          className="input"
          type="text"
          id="hops_name"
          value={hopsTemp.name}
          onChange={(e) => setHopsTemp({ ...hopsTemp, name: e.target.value })}
        />
      </div>
      <div className="formControl">
        <label htmlFor="hops_amount">Hops amount (in grams)</label>
        <input
          className="input"
          type="number"
          id="hops_amount"
          value={hopsTemp.amount.value}
          onChange={(e) =>
            setHopsTemp({
              ...hopsTemp,
              amount: { value: e.target.value, unit: "grams" },
            })
          }
        />
      </div>
      <div className="formControl">
        <label htmlFor="hops_add">Hops add</label>
        <input
          className="input"
          type="text"
          id="hops_add"
          value={hopsTemp.add}
          onChange={(e) => setHopsTemp({ ...hopsTemp, add: e.target.value })}
        />
      </div>
      <div className="formControl">
        <label htmlFor="hops_attribute">Hops attribute</label>
        <input
          className="input"
          type="text"
          id="hops_attribute"
          value={hopsTemp.attribute}
          onChange={(e) =>
            setHopsTemp({ ...hopsTemp, attribute: e.target.value })
          }
        />
      </div>
      <button onClick={handleAddHopsTemp}>Add Hops</button>
    </div>
  );
};

export default HopsForm;
