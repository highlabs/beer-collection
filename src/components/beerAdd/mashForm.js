"use client";
import { useState } from "react";

const MashForm = ({ beer, setBeer }) => {
  const [mashTemp, setMashTemp] = useState({ value: 0, duration: 0 });

  const handleAddMashTemp = (event) => {
    event.preventDefault();

    setBeer({
      ...beer,
      method: {
        ...beer.method,
        mash_temp: [
          ...beer.method.mash_temp,
          {
            temp: { value: mashTemp.value, unit: "celsius" },
            duration: mashTemp.duration,
          },
        ],
      },
    });

    setMashTemp({ value: 0, duration: 0 });
  };

  const handleRemoveMashTemp = (event, index) => {
    event.preventDefault();

    const newMashTemp = [...beer.method.mash_temp];
    newMashTemp.splice(index, 1);
    setBeer({
      ...beer,
      method: {
        ...beer.method,
        mash_temp: newMashTemp,
      },
    });
  };

  return (
    <div>
      <h4>Mash Temp (in celsius)</h4>
      <ul>
        {beer.method.mash_temp.map((mashTemp, index) => (
          <li className="formControl" key={index}>
            <p>
              Temp: {mashTemp.temp.value} / Duration: {mashTemp.duration}
            </p>
            <button onClick={(e) => handleRemoveMashTemp(e, index)}>
              Remove Mash Temp
            </button>
          </li>
        ))}
      </ul>

      <div className="formControl">
        <label htmlFor="mash_temp_value">Temp Value</label>
        <input
          className="input"
          type="number"
          id="mash_temp_value"
          value={mashTemp.value}
          onChange={(e) => setMashTemp({ ...mashTemp, value: e.target.value })}
        />
      </div>
      <div className="formControl">
        <label htmlFor="mash_temp_duration">Duration (in minutes)</label>
        <input
          className="input"
          type="number"
          id="mash_temp_duration"
          value={mashTemp.duration}
          onChange={(e) =>
            setMashTemp({ ...mashTemp, duration: e.target.value })
          }
        />
      </div>

      <button onClick={handleAddMashTemp}>Add Mash Temp</button>
    </div>
  );
};

export default MashForm;
