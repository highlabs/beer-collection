"use client";
import { useState } from "react";

const MaltForm = ({ beer, setBeer }) => {
  const [maltTemp, setMaltTemp] = useState({ name: "", amount: 0 });

  const handleRemoveMalt = (event, index) => {
    event.preventDefault();
    const newMaltTemp = [...beer.ingredients.malt];

    newMaltTemp.splice(index, 1);
    setBeer({
      ...beer,
      ingredients: {
        ...beer.ingredients,
        malt: newMaltTemp,
      },
    });
  };

  const handleAddMaltTemp = (event) => {
    event.preventDefault();

    setBeer({
      ...beer,
      ingredients: {
        ...beer.ingredients,
        malt: [
          ...beer.ingredients.malt,
          {
            name: maltTemp.name,
            amount: { value: maltTemp.amount, unit: "kilograms" },
          },
        ],
      },
    });
    setMaltTemp({ name: "", amount: 0 });
  };

  return (
    <div>
      <h4>Ingredients</h4>
      <div className="formControl">
        <p>
          <strong>Malts</strong>
        </p>
      </div>
      <ul>
        {beer.ingredients.malt.map((malt, index) => (
          <li className="formControl" key={index}>
            <p>{`Name: ${malt.name} / Amount: ${malt.amount.value}kg`}</p>
            <button onClick={(e) => handleRemoveMalt(e, index)}>
              Remove Malt
            </button>
          </li>
        ))}
      </ul>

      <div className="formControl">
        <label htmlFor="malt_name">Malt name</label>
        <input
          className="input"
          type="text"
          id="malt_name"
          value={maltTemp.name}
          onChange={(e) => setMaltTemp({ ...maltTemp, name: e.target.value })}
        />
      </div>
      <div className="formControl">
        <label htmlFor="malt_amount">Malt amount (in kilograms)</label>
        <input
          className="input"
          type="number"
          id="malt_amount"
          value={maltTemp.amount}
          onChange={(e) => setMaltTemp({ ...maltTemp, amount: e.target.value })}
        />
      </div>

      <button onClick={handleAddMaltTemp}>Add Malt</button>
    </div>
  );
};

export default MaltForm;
