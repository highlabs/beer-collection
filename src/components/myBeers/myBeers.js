"use client";
import useStore from "@/store";
import { useBeerStore } from "@/useBeerStore";
import BeerGrid from "@/components/beerGrid";

const MyBeers = () => {
  const beerListing = useStore(useBeerStore, (state) => state.myBeers);
  console.log(beerListing);
  return (
    <main>
      <h1>My beers</h1>
      <BeerGrid noLink beerList={beerListing || []} />
    </main>
  );
};

export default MyBeers;
