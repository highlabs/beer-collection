import { create } from "zustand";
import { persist, createJSONStorage } from "zustand/middleware";

export const useBeerStore = create(
  persist(
    (set, get) => ({
      myBeers: [],
      addBeer: (beer) =>
        set((state) => ({ myBeers: [...state.myBeers, beer] })),
    }),
    {
      name: "myBeers-storage",
      storage: createJSONStorage(() => sessionStorage),
    }
  )
);
