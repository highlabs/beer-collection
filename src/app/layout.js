import { Inter } from "next/font/google";
import "./globals.css";
import Link from "next/link";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Beer Collection",
  description: "Your personal beer collection",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <header>
          <nav>
            <Link href="/">Beer Collection</Link>
            <div>
              <Link href="/add">Add Beer</Link>
              <Link href="/my_beers">Show My Beers</Link>
            </div>
          </nav>
        </header>
        {children}
      </body>
    </html>
  );
}
