import styles from "./page.module.css";
import BeerContainer from "@/components/beerContainer";

export default async function Home() {
  const data = await getData();

  return (
    <main className={styles.main}>
      <BeerContainer data={data} />
    </main>
  );
}

async function getData() {
  const res = await fetch("https://api.punkapi.com/v2/beers");

  if (!res.ok) {
    throw new Error(res.statusText);
  }

  return res.json();
}
