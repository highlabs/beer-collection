import MyBeers from "@/components/myBeers";

const My_Beers = () => {
  return (
    <main>
      <MyBeers />
    </main>
  );
};

export default My_Beers;
