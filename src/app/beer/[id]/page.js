import BeerInfo from "@/components/beerInfo";

export default async function Page({ params }) {
  const res = await getData(params.id);

  return (
    <main>
      <BeerInfo beer={res[0]} showImage />
    </main>
  );
}

async function getData(id) {
  const res = await fetch(`https://api.punkapi.com/v2/beers/${id}`);

  if (!res.ok) {
    throw new Error(res.statusText);
  }

  return res.json();
}
