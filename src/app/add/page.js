import BeerAdd from "@/components/beerAdd";

const BeerAddPage = () => {
  return (
    <main>
      <BeerAdd />
    </main>
  );
};

export default BeerAddPage;
