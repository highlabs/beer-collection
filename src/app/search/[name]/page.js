import styles from "../../page.module.css";
import BeerGrid from "@/components/beerGrid";
import Sidebar from "@/components/sidebar";

export default async function Search({ params }) {
  const data = await getData(params.name);

  const searchBeer = (params) => {
    console.log(params);
  };

  return (
    <main className={styles.main}>
      <Sidebar onSearch={searchBeer} />
      <BeerGrid beerList={data} />
    </main>
  );
}

async function getData(name) {
  const res = await fetch(`https://api.punkapi.com/v2/beers?beer_name=${name}`);

  if (!res.ok) {
    throw new Error(res.statusText);
  }

  return res.json();
}
